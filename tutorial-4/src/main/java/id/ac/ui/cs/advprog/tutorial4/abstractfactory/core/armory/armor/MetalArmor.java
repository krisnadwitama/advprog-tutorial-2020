package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class MetalArmor implements Armor {

    @Override
    public String getName() {
        return "Metal armor";
    }

    @Override
    public String getDescription() {
        return "This is a metal armor";
    }
}
