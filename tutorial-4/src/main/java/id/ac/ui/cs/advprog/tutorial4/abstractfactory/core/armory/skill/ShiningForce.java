package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill;

public class ShiningForce implements Skill {

    @Override
    public String getName() {
        return "Shining force";
    }

    @Override
    public String getDescription() {
        return "This is shining force";
    }
}
