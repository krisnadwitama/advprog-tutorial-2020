package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.armor;

public class ShiningArmor implements Armor {

    @Override
    public String getName() {
        return "Shining armor";
    }

    @Override
    public String getDescription() {
        return "This is a shining armor";
    }
}
