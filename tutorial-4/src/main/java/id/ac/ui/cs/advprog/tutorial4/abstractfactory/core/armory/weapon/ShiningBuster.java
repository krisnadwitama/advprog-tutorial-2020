package id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon;

public class ShiningBuster implements Weapon {

    @Override
    public String getName() {
        return "Shining buster";
    }

    @Override
    public String getDescription() {
        return "This is a shining buster";
    }
}
