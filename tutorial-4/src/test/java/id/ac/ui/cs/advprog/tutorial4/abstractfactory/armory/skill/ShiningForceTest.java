package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.skill;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.ShiningForce;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.skill.Skill;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ShiningForceTest {

    Skill shiningForce;

    @BeforeEach
    public void setUp(){
        shiningForce = new ShiningForce();
    }

    @Test
    public void testToString(){
        assertEquals("Shining force", shiningForce.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("This is shining force", shiningForce.getDescription());
    }
}
