package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.HolyWish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    @Mock
    private HolyGrail holyGrail;

    @Test
    public void testGetAWish() {
        Mockito.when(holyGrail.getHolyWish()).thenReturn(HolyWish.getInstance());
        assertTrue(holyGrail.getHolyWish() instanceof HolyWish);
    }

}
