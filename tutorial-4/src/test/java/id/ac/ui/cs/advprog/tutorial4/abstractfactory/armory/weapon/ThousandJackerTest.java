package id.ac.ui.cs.advprog.tutorial4.abstractfactory.armory.weapon;

import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.ThousandJacker;
import id.ac.ui.cs.advprog.tutorial4.abstractfactory.core.armory.weapon.Weapon;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ThousandJackerTest {

    Weapon thousandJacker;

    @BeforeEach
    public void setUp(){
        thousandJacker = new ThousandJacker();
    }

    @Test
    public void testToString(){
        assertEquals("Thousand jacker", thousandJacker.getName());
    }

    @Test
    public void testDescription(){
        assertEquals("This is a thousand jacker", thousandJacker.getDescription());
    }
}
