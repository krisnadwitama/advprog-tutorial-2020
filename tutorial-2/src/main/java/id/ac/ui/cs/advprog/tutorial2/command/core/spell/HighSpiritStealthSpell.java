package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritStealthSpell extends HighSpiritSpell {
    public HighSpiritStealthSpell(HighSpirit highSpirit) {
        super(highSpirit);
    }

    @Override
    public void cast() {
        this.spirit.stealthStance();
    }

    public String spellName() {
        return this.spirit.getRace() + ":Stealth";
    }
}
