package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithSword implements AttackBehavior {
        //ToDo: Complete me
        public AttackWithSword() {

        }

        public String attack() {
            return "I use Sword";
        }

        public String getType() {
        	return "AttackWithSword";
        }
}
