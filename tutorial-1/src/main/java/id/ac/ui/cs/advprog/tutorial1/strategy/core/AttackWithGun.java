package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {
        //ToDo: Complete me
        public AttackWithGun() {

        }

        public String attack() {
            return "I use Gun";
        }

        public String getType() {
        	return "AttackWithGun";
        }
}
