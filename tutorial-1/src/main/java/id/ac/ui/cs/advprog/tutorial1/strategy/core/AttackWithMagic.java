package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
        //ToDo: Complete me
        public AttackWithMagic() {

        }

        public String attack() {
            return "I use Magic";
        }

        public String getType() {
                return "AttackWithMagic";
        }
}
