package id.ac.ui.cs.advprog.tutorial1.strategy.core;

import id.ac.ui.cs.advprog.tutorial1.strategy.repository.AdventurerRepository;

public class AgileAdventurer extends Adventurer {
    //ToDo: Complete me
    public AgileAdventurer() {
        this.setAttackBehavior(new AttackWithGun());
        this.setDefenseBehavior(new DefendWithBarrier());
    }

    @Override
    public String getAlias() {
        return "I'am Agile";
    }
}
