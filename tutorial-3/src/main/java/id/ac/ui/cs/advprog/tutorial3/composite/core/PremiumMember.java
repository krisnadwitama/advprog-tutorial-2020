package id.ac.ui.cs.advprog.tutorial3.composite.core;

import java.util.ArrayList;
import java.util.List;

public class PremiumMember implements Member {
    //TODO: Complete me
    private String name;
    private String role;
    private List<Member> listMember = new ArrayList<Member>();

    public PremiumMember(String name, String role) {
        this.name = name;
        this.role = role;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getRole() {
        return this.role;
    }

    @Override
    public void addChildMember(Member member) {
        if(this.role.equals("Master")) {
            this.listMember.add(member);
        }
        else if(listMember.size() < 3) {
            this.listMember.add(member);
        }
    }

    @Override
    public void removeChildMember(Member member) {
        this.listMember.remove(member);
    }

    @Override
    public List<Member> getChildMembers() {
        return listMember;
    }
}
