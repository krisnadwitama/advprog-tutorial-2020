package id.ac.ui.cs.advprog.tutorial3.composite.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PremiumMemberTest {
    private Member member;

    @BeforeEach
    public void setUp() {
        member = new PremiumMember("Wati", "Gold Merchant");
    }

    @Test
    public void testMethodGetName()
    {
        //TODO: Complete me
        assertEquals("Wati", member.getName());
    }

    @Test
    public void testMethodGetRole() {
        //TODO: Complete me
        assertEquals("Gold Merchant", member.getRole());
    }

    @Test
    public void testMethodAddChildMember() {
        //TODO: Complete me
        Member anak = new OrdinaryMember("Tama", "Merchant");
        member.addChildMember(anak);
        assertEquals(1, member.getChildMembers().size());
    }

    @Test
    public void testMethodRemoveChildMember() {
        //TODO: Complete me
        Member anak = new OrdinaryMember("Tama", "Merchant");
        member.addChildMember(anak);
        member.removeChildMember(anak);
        assertEquals(0, member.getChildMembers().size());
    }

    @Test
    public void testNonGuildMasterCanNotAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member anak1 = new PremiumMember("I", "Premium");
        Member anak2 = new PremiumMember("Made", "Premium");
        Member anak3 = new PremiumMember("Krisna", "Premium");
        Member anak4 = new PremiumMember("Dwitama", "Premium");
        member.addChildMember(anak1);
        member.addChildMember(anak2);
        member.addChildMember(anak3);
        member.addChildMember(anak4);
        assertEquals(3, member.getChildMembers().size());
    }

    @Test
    public void testGuildMasterCanAddChildMembersMoreThanThree() {
        //TODO: Complete me
        Member guildMaster = new PremiumMember("Budi", "Master");
        Guild guild = new Guild(guildMaster);
        Member anak1 = new PremiumMember("I", "Premium");
        Member anak2 = new PremiumMember("Made", "Premium");
        Member anak3 = new PremiumMember("Krisna", "Premium");
        Member anak4 = new PremiumMember("Dwitama", "Premium");
        guildMaster.addChildMember(anak1);
        guildMaster.addChildMember(anak2);
        guildMaster.addChildMember(anak3);
        guildMaster.addChildMember(anak4);
        assertEquals(4, guildMaster.getChildMembers().size());
    }
}
