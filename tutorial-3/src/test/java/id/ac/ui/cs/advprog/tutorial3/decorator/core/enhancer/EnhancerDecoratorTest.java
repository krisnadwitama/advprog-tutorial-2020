package id.ac.ui.cs.advprog.tutorial3.decorator.core.enhancer;

import id.ac.ui.cs.advprog.tutorial3.decorator.core.weapon.Weapon;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;



public class EnhancerDecoratorTest {

    Weapon weapon1;
    Weapon weapon2;
    Weapon weapon3;
    Weapon weapon4;
    Weapon weapon5;
    @Test
    public void testAddWeaponEnhancement(){

        weapon1 = EnhancerDecorator.CHAOS_UPGRADE.addWeaponEnhancement(weapon1);
        weapon2 = EnhancerDecorator.MAGIC_UPGRADE.addWeaponEnhancement(weapon2);
        weapon3 = EnhancerDecorator.REGULAR_UPGRADE.addWeaponEnhancement(weapon3);
        weapon4 = EnhancerDecorator.RAW_UPGRADE.addWeaponEnhancement(weapon4);
        weapon5 = EnhancerDecorator.UNIQUE_UPGRADE.addWeaponEnhancement(weapon5);

        //TODO: Complete me
        int value1 = weapon1.getWeaponValue();
        int value2 = weapon2.getWeaponValue();
        int value3 = weapon3.getWeaponValue();
        int value4 = weapon4.getWeaponValue();
        int value5 = weapon5.getWeaponValue();
        assertTrue(value1 >= 50 && value1 <= 55);
        assertTrue(value2 >= 15 && value2 <= 20);
        assertTrue(value3 >= 1 && value3 <= 5);
        assertTrue(value4 >= 5 && value4 <= 10);
        assertTrue(value5 >= 10 && value5 <= 15);
    }

}
